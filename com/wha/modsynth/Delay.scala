package com.wha.modsynth;

import de.gulden.framework.jjack.JJackSystem

class Delay (n: String) extends Module {
  val name = n
  controls += "time" -> 250.0
  controls += "mixSig" -> 75.0
  controls += "mixFx" -> 50.0
  controls += "outSig" -> 75.0
  controls += "outFx" -> 50.0
  var ring: List[FloatRingBuffer] = Nil
  
  def computePut(mixSig: Float, mixFx: Float)(sig: Float)(fx: Float): Float = {
    val x = sig * mixSig + fx * mixFx
    //println (x + " ")
    x
  }

  override def transformAudio(buf: List[List[float]]): List[List[float]] = {
    val sampleRate = JJackSystem.getSampleRate
    val time = controls("time")
    val diff = (time * sampleRate / 1000).toInt
    if (ring == Nil) List.range(0, buf.size).foreach(x => ring = new FloatRingBuffer(diff) :: ring)
    else ring.foreach(x => x.resize(diff))

    val mixSig = controls("mixSig") / 100
    val mixFx = controls("mixFx") / 100
    val outSig = controls("outSig") / 100
    val outFx = controls("outFx") / 100
    val compFxMix = computePut(mixSig.toFloat, mixFx.toFloat)_
    val compSigMix = computePut(outSig.toFloat, outFx.toFloat)_
    for (channel <- buf; ringbuf <- ring)
      yield {
      var i = 0
      for (sample <- channel)
        yield {
          //if (i == 0) {println(sample + " "); i = 1}
          val compFx = compFxMix(sample)
          val compSig = compSigMix(sample)
          compSig(ringbuf.getPut(compFx))
      }
    }
  }

}
