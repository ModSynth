package com.wha.modsynth;

import de.gulden.framework.jjack._;

class TestJackC extends JJackAudioProcessor {
  def process(e: JJackAudioEvent) {
    println("here")
    List.range(0, e.countChannels).foreach(i => {
      val in = e.getInput(i)
      val out = e.getOutput(i)
      List.range(0, in.capacity).foreach(j => out.put(in.get(j)))
    })
  }
}

object TestJack {
  def main(args : Array[String]) : Unit = {
    val tj = new TestJackC
    JJackSystem.setProcessor(tj);
    wait(20000);
  }
}
