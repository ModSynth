package com.wha.modsynth;

import scala.collection.mutable._

class Canvas {
  var modules:Map[String, Module] = new HashMap[String, Module]

  def add(module: Module) {
    modules += module.name -> module
  }
  
  def get(name: String): Module = modules(name)
  
  def set(modName: String, controlName: String, v: Double) {
    println(modName + ":" + controlName + ":" + v)
    if (modules contains modName) {
      get(modName) ! new ControlSig(controlName, v)
    }
  }
}
