package com.wha.modsynth;

import de.gulden.framework.jjack._

import java.io._

object Test {
  def main(args : Array[String]) : Unit = {
    val canvas = new Canvas()
    val jackIn = new JackIO()

    val gain = new Gain("Gain1")
    canvas.add(gain)
    
    val delay = new Delay("Delay1")
    canvas.add(delay)

    jackIn.start()
    jackIn ! new Connect(gain)

    gain.start()
    gain ! new Connect(delay)

    delay.start()
    delay ! new Connect(jackIn)
    
    JJackSystem.setProcessor(jackIn.jackProcessor);
    println("after jjacksystem setProcessor")

    val f = new BufferedReader(new InputStreamReader(java.lang.System.in))
    while (true) {
      print ("> ")
      val s = f.readLine()
      val toks = s split " "
      val v = toks(2).toDouble
      canvas.set(toks(0), toks(1), v)
    }
  }
}
