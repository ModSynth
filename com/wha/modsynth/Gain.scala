package com.wha.modsynth;

case class Gain(n: String) extends Module {
  val name = n
  controls += "gain" -> 1.0
  
  override def transformAudio(buf: List[List[float]]): List[List[float]] = {
    val gain = controls("gain")
    buf.map(channel => channel.map(sample => (gain * sample).toFloat))
  }

}
