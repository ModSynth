package com.wha.modsynth;

import scala.actors._
import scala.actors.Actor._
import scala.collection.mutable._

trait Connector
case class Connect(client: Module) extends Connector
case class Disconnect(client: Module) extends Connector

trait Signal
case class ControlSig(name: String, f: Double) extends Signal
case class Audio(buf: List[List[float]]) extends Signal

trait Module extends Actor {
  var input: Module = _
  var outputs:List[Module] = Nil
  var controls:Map[String, Double] = new HashMap[String, Double]
  var channels: Int = _
  val name: String
  
  def log(s: String): Unit = () // println(name + ": " + s)
  
  def act() {
    while(true) {
      log("in act")
      receive {
      case Connect(client) => {outputs = client :: outputs; log("Output client: " + client.name)}
      case Disconnect(client) => {outputs = outputs filter(x => x != client); log("Output client: " + client.name)}
      case Audio(buf) => {doAudio(buf) ; log("Audio(client)")}
      case ControlSig(name, f) => {doControl(name, f) ; log("Audio(client)"); reply(new ControlSig(name, f))}
      }
    }
  }

  def doAudio(buf: List[List[float]]) =  {
    log("in doAudio")
    val b = transformAudio(buf)
    outputs foreach (x => {log("sending audio to " + x.name); x ! new Audio(b)})
  }
  
  def doControl(name: String, f: Double) {
    controls += name -> f
  }
  
  // a null definition since no inputs
  def transformAudio(buf: List[List[float]]): List[List[float]] = buf

}
case class CopyModule(n: String) extends Module {
  val name = n
}
