package com.wha.modsynth;

import scala.collection.mutable.Queue

/**
  * this code is a modification of the scala examples Bounded Buffer with a resize
  * operation added.
  */

abstract class RingBuffer[A] (size: Int) {
  var N = size
  val queue = new Queue[A]

  def getPut(pf:(A => A)): A = {
    val x = get
    put(pf(x))
    x
  }
  
  def put(x: A) = {
    queue += x
  }

  def default: A
  
  def get: A = {
    val s = queue.size
    if (s < N) {val x = default; x} 
    else if (N < s) {List.range(N, s-1).foreach(x => queue.dequeue); queue.dequeue} 
    else queue.dequeue
  }
  
  def resize(size: Int) = {
     N = size
  }
}

class FloatRingBuffer(size: Int) extends RingBuffer[Float](size: Int) {
  def default: Float = 0.0f
}

