package com.wha.modsynth;
import scala.actors._
import scala.actors.Actor._
import scala.concurrent.SyncVar;

import de.gulden.framework.jjack._;

class JackIO extends Module {
  var inBuf = new SyncVar[List[List[float]]]
  var outBuf = new SyncVar[List[List[float]]]
  
  val name = "JackIn"
    
  val getSampleRate = JJackSystem.getSampleRate
  
  class JackProcessor(ji: Module) extends JJackAudioProcessor with Actor {
    override def process(e: JJackAudioEvent) = {
      log("in process")
      channels = e.countChannels
      outBuf.unset
      inBuf.set(for (i <- List.range(0, channels)) 
        yield { 
          val in = e.getInput(i)
          for (j <- List.range(0, in.capacity)) yield in.get(j)})
      ji ! new Audio(inBuf.get)
      doOut(e, outBuf.get)
    }
    
    def act() {
      
    }
  }
  
  val jackProcessor = new JackProcessor(this)
  
  def doOut(e: JJackAudioEvent, b: List[List[float]]) : Unit = {
    log("in doOut")
    channels = e.countChannels
    List.range(0, channels).foreach(i => {
      log("output channel: " + i)
      val out = e.getOutput(i)
      b(i).foreach(sample => {out.put(sample)})}) 
  }

  override def doAudio(buf: List[List[float]]) =  {
    log("in doAudio " + inBuf.isSet)
    if (inBuf.isSet) {
      val b = transformAudio(buf)
      inBuf.unset
      outputs foreach (x => {log("sending audio to " + x.name); x ! new Audio(b)})
    }
    else {
      outBuf.set(buf)  
    }
  }

}
