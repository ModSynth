package com.wha.modsynth_test;

import concurrent.ops._

import com.wha.modsynth.FloatRingBuffer

object TestRingBuffer {
  def kill(delay: Int) = new java.util.Timer().schedule(
      new java.util.TimerTask {
        override def run() = {
          println("[killed]")
          System.exit(0)
        }
      },
      delay) // in milliseconds

  def computePut(sig: Float)(x: Float): Float = {
    sig + x * 0.1f    
  }
  
  def output(buf: FloatRingBuffer, sig: Float) {
    val fput = computePut(sig)_
    val fx = buf.getPut(fput) 
    println(sig + fx * 0.5f)
  }
  
  def main(args: Array[String]) {
    val buf = new FloatRingBuffer(100)
    var cnt = 100.0f
    def produceString = { cnt }
    def consumeString(ss: Float) = println(ss)
    List.range(1, 10).foreach(x => {
      val sig = 100.0f
      output(buf, sig)
    })
    List.range(1, 1000).foreach(x => {
      val sig = 0.0f
      output(buf, sig)
    })
  }

}
