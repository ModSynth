package com.wha.modsynth_test;

import com.wha.modsynth._

object TestDelay {
  def main(args : Array[String]) : Unit = {
    val delay = new Delay("Delay1")
    delay.channels = 2
    delay.start()
    List(1.0, 2.0, 3.0, 5.0).foreach ( time => {
      delay !? new ControlSig("time", time)
      val temp1 = for(x <- List.range(1, 10)) yield 100.0f
      val temp2 = temp1 ::: {for(x <- List.range(1, 1000)) yield 0.0f}
      val input = List(temp2, temp2)
      val output = delay.transformAudio(input)
      output.foreach(x => {x.foreach(sample => print(sample + ",")); println})
    })
  }
}
